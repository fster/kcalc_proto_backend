const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// Instantiate the Express application.
const app = express();

//This code sets the origin to the URL that this serverapplications listens to.
var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//instantiating database with requirements based on datamodels.
const db = require("./app/models");

db.sequelize.sync();


//db.sequelize.sync({ force: true }).then(() => {
//    console.log("Drop and re-sync db.");
//});

// simple test route to check if server is running via browser.
//app.get("/", (req, res) => {
//    res.json({ message: "Welcome. The server is running." });
//});

//Here the code tells the application to use the defined routes.
require("./app/routes/store.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
