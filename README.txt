# Kcalculation Vue Prototype

The prototype is a very small scoped project made for showcasing basic Vue concepts to
the product owners of Kcalculation, as part of my short internship period.
It contains the functionality required by the Kcalculation products most
essential use case - the ability to search a database with certain conditions.
**This is the server application listening to the Vue application.

### Setup guide

- Further development might require downloading Node.js and installing necessary
modules in the project directory.
After navigating to the directory, run the CLI command: 

npm install express sequelize mysql2 body-parser cors --save


- You will also need to setup a mySQL database table named 'stores' with the
 following structure:

+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | int unsigned | NO   | PRI | NULL    | auto_increment |
| name       | varchar(150) | NO   |     | NULL    |                |
| address    | varchar(150) | NO   |     | NULL    |                |
| postalcode | varchar(150) | NO   |     | NULL    |                |
| image      | blob         | YES  |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+

Then open the 'db.config.js' file of the project and change 'HOST', 'USER',
'PASSWORD', 'DB' to values appropriate for your own mySQL configuration.


- Lastly launching this application by CLI command.
Navigate to project directory and run:

node server.js




License
----

MIT

Copyright 2020 Kristoffer Alexander Fich

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


