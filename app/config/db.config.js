// Here is where the configuration for the MYSQL database is defined.
// Add your own configuration to your database with the table defined to fit the systems ORM. //  
module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "passwordhere",
    DB: "dbname",
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};