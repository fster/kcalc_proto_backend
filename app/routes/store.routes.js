module.exports = app => {
    const stores = require("../controllers/store.controller.js");

    var router = require("express").Router();

    router.get("/", stores.findAll);


    app.use('/api/stores', router);
};