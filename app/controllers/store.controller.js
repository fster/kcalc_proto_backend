const db = require("../models");
const Store = db.stores;
const Op = db.Sequelize.Op;

//This code defines a method for responding to a certain request, and contacting the database for appropriate response.

exports.findAll = (req, res) => {
    const postalcode = req.query.postalcode; 
    var condition = postalcode ? { postalcode: { [Op.like]: `%${postalcode}%` } } : null;

    Store.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error has occurred."
            });
        });

};



