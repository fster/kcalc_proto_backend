const dbConfig = require("../config/db.config.js");

// This code initializes the object relational mapping framework used to communicate with the database, and then tells the database connection variable to use it.
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorAliases: 0, 
    define: {
        timestamps: false
    },

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});


const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;


db.stores = require("./store.model.js")(sequelize, Sequelize);

module.exports = db;