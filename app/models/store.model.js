// This code defines the object we want the ORM framework to handle. This should respond to the columns in the database.
module.exports = (sequelize, Sequelize) => {
    const Store = sequelize.define("store", {
        name: {
            type: Sequelize.STRING
        },
        address: {
            type: Sequelize.STRING
        },
        postalcode: {
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.BLOB('long')
            
        }

    });

    return Store;
};